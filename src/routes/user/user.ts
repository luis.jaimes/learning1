import { Request, Response, Router } from "express";
import { userSchema } from "../../models/User";

class UserRoutes {
    userRouter:Router

    constructor() {
        this.userRouter = Router();
        this.routes()
    }
    async getUsers(req: Request, res: Response ) {
        const users = await userSchema.find();
        res.json({data: users})
    }
    async getUserById(req: Request, res: Response ) {
        if (!req.params.id) res.status(400).json({ message: "param (id) not provided" })
        const users = await userSchema.findById(req.params.id);
        res.json({data: users})
    }
    async createUser(req: Request, res: Response ) {
    const { firstName, lastName, email, image, password } = req.body
    const newUser = new userSchema({ firstName, lastName, email, image, password })
    const userSaved = await newUser.save()
    
    res.send({data: userSaved})
    }
    async updateUser(req: Request, res: Response ) {
        if (!req.params.id) res.status(400).json({ message: "param (id) not provided" })
        const userUpdated = await userSchema.findByIdAndUpdate(req.params.id, req.body, { new: true });
        res.json({data: userUpdated})
    }
    async disableUser(req: Request, res: Response ) { 
        if (!req.params.id) res.status(400).json({ message: "param (id) not provided" })
        const userDisabled = await userSchema.findByIdAndUpdate(req.params.id, { disabled: true }, { new: true });
        res.json({data: userDisabled})
    }
    async deleteUser(req: Request, res: Response ) { 
        if (!req.params.id) res.status(400).json({ message: "param (id) not provided" })
        const userDeleted = await userSchema.findByIdAndUpdate(req.params.id, { removed: true }, { new: true });
        res.json({data: userDeleted})
    }

    routes() {
        this.userRouter.get("/", this.getUsers)
        this.userRouter.get("/:id", this.getUserById)
        this.userRouter.post("/", this.createUser)
        this.userRouter.put("/:id", this.updateUser)
        this.userRouter.put("/disable/:id", this.disableUser)
        this.userRouter.delete("/remove/:id", this.deleteUser)
    }
}

const userRoutes = new UserRoutes();
// userRoutes.routes();
const { userRouter } = userRoutes
export { userRouter }
