import { Request, Response, Router } from "express";
import { postSchema } from "../../models/Post";
import { addPostInCategory } from "../category/categoriesUtil";

class PostRoutes {
    postRouter:Router

    constructor() {
        this.postRouter = Router();
        this.routes()
    }
    async getPosts(req: Request, res: Response ) {
        const posts = await postSchema.find().populate("categoryId");
        res.json({data: posts})
    }
    async getPostById(req: Request, res: Response ) {
        if (!req.params.id) res.status(400).json({ message: "param (id) not provided" })
        const posts = await postSchema.findById(req.params.id).populate("categoryId");
        res.json({data: posts})
    }
    async createPost(req: Request, res: Response ) {
    const newPost = new postSchema(req.body)
    const postSaved = await newPost.save()
    req.body.categoryId && await addPostInCategory(postSaved._id.toString(), req.body.categoryId, res)
    res.send({data: postSaved})
    }
    async updatePost(req: Request, res: Response ) {
        if (!req.params.id) res.status(400).json({ message: "param (id) not provided" })
        const postUpdated = await postSchema.findByIdAndUpdate(req.params.id, req.body, { new: true });
        res.json({data: postUpdated})
    }
    async deletePost(req: Request, res: Response ) { 
        if (!req.params.id) res.status(400).json({ message: "param (id) not provided" })
        const postDeleted = await postSchema.findByIdAndDelete(req.params.id);
        res.json({data: postDeleted})
    }

    routes() {
        this.postRouter.get("/", this.getPosts)
        this.postRouter.get("/:id", this.getPostById)
        this.postRouter.post("/", this.createPost)
        this.postRouter.put("/:id", this.updatePost)
        this.postRouter.delete("/remove/:id", this.deletePost)
    }
}

const postRoutes = new PostRoutes();
// postRoutes.routes();
const { postRouter } = postRoutes
export { postRouter }
