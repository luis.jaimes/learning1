import { Request, Response, Router } from "express";
import { categorySchema } from "../../models/Category";
import { addPostInCategory } from "./categoriesUtil";

class CategoryRoutes {
    categoryRouter:Router

    constructor() {
        this.categoryRouter = Router();
        this.routes()
    }
    async getCategories(req: Request, res: Response ) {
        const categories = await categorySchema.find().populate("posts");
        res.json({data: categories})
    }
    async getCategoryById(req: Request, res: Response ) {
        if (!req.params.id) res.status(400).json({ message: "param (id) not provided" })
        const categories = await categorySchema.findById(req.params.id).populate("posts");
        res.json({data: categories})
    }
    async createCategory(req: Request, res: Response ) {
    const newCategory = new categorySchema(req.body)
    const categorySaved = await newCategory.save()
    res.send({data: categorySaved})
    }
    async updateCategory(req: Request, res: Response ) {
        if (!req.params.id) res.status(400).json({ message: "param (id) not provided" })
        req.body.posts && req.body.posts.forEach( async (element: String) => {
            await addPostInCategory(element, req.params.id, res)
        });
        const categoryUpdated = await categorySchema.findByIdAndUpdate(req.params.id, req.body, { new: true });
        res.json({data: categoryUpdated})
    }
    async deleteCategory(req: Request, res: Response ) { 
        if (!req.params.id) res.status(400).json({ message: "param (id) not provided" })
        const categoryDeleted = await categorySchema.findByIdAndDelete(req.params.id);
        res.json({data: categoryDeleted})
    }

    routes() {
        this.categoryRouter.get("/", this.getCategories)
        this.categoryRouter.get("/:id", this.getCategoryById)
        this.categoryRouter.post("/", this.createCategory)
        this.categoryRouter.put("/:id", this.updateCategory)
        this.categoryRouter.delete("/remove/:id", this.deleteCategory)
    }
}

const categoryRoutes = new CategoryRoutes();
// categoryRoutes.routes();
const { categoryRouter } = categoryRoutes
export { categoryRouter }
