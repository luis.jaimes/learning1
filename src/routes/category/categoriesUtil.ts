import { Response } from "express";
import { categorySchema } from "../../models/Category";

const addPostInCategory = async (postId: String, categoryId: String, res: Response ) => {
    if (!postId || !categoryId) res.status(400).json({ message: "param (postId or categoryId) not provided" })
    const category = await categorySchema.findById(categoryId);
    const isPostAlreadyAdded = category?.posts && category?.posts.find((element) => element.toString() === postId)
    !isPostAlreadyAdded && await categorySchema.findByIdAndUpdate(categoryId, { $push: { posts: postId } }, { new: true });    
}

export { addPostInCategory }