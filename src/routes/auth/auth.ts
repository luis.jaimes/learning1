import { Request, Response, Router } from "express";
import { userSchema } from "../../models/User";
import { getAccessToken } from "../../utils/token/token";
import bcrypt from "bcryptjs"

class AuthRoutes {
    authRouter: Router

    constructor() {
        this.authRouter = Router();
        this.routes();
    }

    async signup (req: Request, res: Response) {
        try {
            const { image, firstName, lastName, email, password } = req.body;
            const salt = await bcrypt.genSalt(10);
            const passwordEncrypted = await bcrypt.hash(password, salt);
            const newUser = new userSchema({ firstName, lastName, image, email, password: passwordEncrypted });
            const userSaved = await newUser.save()
            const accessToken = getAccessToken(userSaved._id)

            return res.header({"auth-token": accessToken}).json({ data: userSaved });
        } catch (error) {
            return res.status(401).json(error);
            
        }
    }

    async signin (req: Request, res: Response) {
        const user = await userSchema.findOne({ email: req.body.email})
        if(!user) return res.status(404).json("user not found");
        const isPasswordValid = bcrypt.compare(req.body.password, user.password)
        if(!isPasswordValid) return res.status(404).json("password wrong");

        const accessToken = getAccessToken(user._id);
        return res.header({ "auth-token": accessToken }).json( { data: user })
    }
    
    routes () {
        this.authRouter.post("/signup", this.signup);
        this.authRouter.post("/signin", this.signin);
    }
}

const authRoutes = new AuthRoutes();
const { authRouter } = authRoutes

export { authRouter }