import {Schema, model} from "mongoose";

const UserSchema = new Schema({
    firstName: {type: String, required: true},
    lastName: String,
    email: {type: String, required: true, unique: true, lowercase: true},
    password: {type: String, required: true },
    image: {
        baseUrl: { type: String, required: true },
        url: { type: String, required: true },
        path: { type: String, required: true },
        name: { type: String, required: true },
        type: { type: String, required: true },
    },
    removed: {type: Boolean, default: false },
    disabled: {type: Boolean, default: false },
}, { timestamps: true })

const userSchema = model("User", UserSchema)

export {userSchema}