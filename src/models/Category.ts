import {Schema, model} from "mongoose";

const CategorySchema = new Schema({
    name: {type: String, required: true},
    posts: [{
        type: Schema.Types.ObjectId,
        ref: "Post"
    }],
}, { timestamps: true })

const categorySchema = model("Category", CategorySchema)

export {categorySchema}