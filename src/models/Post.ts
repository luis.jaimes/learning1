import {Schema, model} from "mongoose";

const PostSchema = new Schema({
    title: {type: String, required: true},
    description: String,
    categoryId: { type: Schema.Types.ObjectId, ref: "Category" },
    image: {
        baseUrl: { type: String, required: true },
        url: { type: String, required: true },
        path: { type: String, required: true },
        name: { type: String, required: true },
        type: { type: String, required: true },
    },
}, { timestamps: true })

const postSchema = model("Post", PostSchema)

export {postSchema}