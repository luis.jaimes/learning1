import { Request, Response, NextFunction } from "express"
import jwt from "jsonwebtoken";
import { Types } from "mongoose";
import { userSchema } from "../../models/User"

interface IPayload {
    _id: string
    iat: number
    exp: number
}

const getAccessToken = (userId: Types.ObjectId) => {
    const token = jwt.sign({_id: userId}, process.env.SECRET_JWT || "SECRET_JWT", {
        expiresIn: 60 * 60 * 24
    })

    return token;
}

const TokenValidation = async (req: Request, res: Response, next: NextFunction) => {
    try {
        if(req.path === "/api/auth/signup" || req.path === "/api/auth/signin") return next();

        const token = req.header("auth-token");

        if(!token) return res.status(401).json("Token failed");

        const jwtPayload = jwt.verify(token, process.env.SECRET_JWT || "SECRET_JWT") as IPayload;
        const user = await userSchema.findById(jwtPayload._id)

        if(!user) return res.status(401).json({message: "user not found"})

        return next();

    } catch (error) {
        
    }
}

export {
    getAccessToken,
    TokenValidation
}