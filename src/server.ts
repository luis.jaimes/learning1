import express from "express";
import morgan from "morgan";
import helmet from "helmet";
import { userRouter } from "./routes/user/user";
import mongoose from "mongoose";
import compression from "compression";
import cors from "cors";
import 'dotenv/config'
import { categoryRouter } from "./routes/category/category";
import { postRouter } from "./routes/post/post";
import { TokenValidation } from "./utils/token/token";
import { authRouter } from "./routes/auth/auth";

require("./solid/1-Srp")
require("./solid/2-Open-close")
require("./solid/3-Liskov-substitution")
require("./solid/4-Interface-Segregation")
require("./solid/5-Dependecy-Inversion")


class Server {
    public app: express.Application;
    constructor() {
        this.app = express();
        this.config();
        this.routes();
    }

    config() {

        // Mongo DB Connection
        // I want to give a little more context to an existing answers. When strict option is set to true, Mongoose will ensure that only the fields that are specified in your Schema will be saved in the database.
        mongoose.set('strictQuery', true);
        // Si no conecta checar la IP PUBLICA y la URI
        process.env.MONGODB_URI ? 
        mongoose.connect(process.env.MONGODB_URI).then(()=>{console.log('Conectada')})
         : console.log("MONGO DB NO CONNECTED: revisar IP PUBLICA y revisar URI de conexión");
        
        //  Settings
        this.app.set("port", process.env.PORT || 4000);
        // Middlewares
        this.app.use(morgan("dev"));
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended: false}))
        this.app.use(helmet());
        this.app.use(compression());
        this.app.use(cors());
        this.app.use(TokenValidation);
    }

    routes() {
        this.app.use("/api/users", userRouter);
        this.app.use("/api/categories", categoryRouter);
        this.app.use("/api/posts", postRouter);
        this.app.use("/api/auth", authRouter);

    }

    start() {
        this.app.listen(this.app.get("port"), ()=>{
            console.log("Server on PORT", this.app.get("port"));
        })
    }
}

const server = new Server();
server.start()