import axios from 'axios';

// - Principio de Open Closed - Abierta para la extensión pero cerrada para la modificación
// Se creó una clase separada para manejar el cliente Axios (HttpClient). Con esto las clases { TodoService, PostService, 
// PhotosService } no dependen de axios directamente.
// Este ejercicio es solo una representación de metodos o clases para mostrar un ejemplo

// clase para manejar todo lo que tenga que ver por el lado del cliente
class HttpClient {
    async get( url: string ) {
        const { data, status } = await axios.get(url);
        console.log({ status })
        return { data, status };   
    }
}

(async () => {

    // clases para llamar diferentes servicios, TODO, POST y PHOTOS usando el mismo HttpClient

    class TodoService { 

        constructor( private http: HttpClient){}

        async getTodoItems() {
            const { data } = await this.http.get('https://jsonplaceholder.typicode.com/todos/');
            return data;
        }
    }


    class PostService {
        constructor( private http: HttpClient){
                    
                }
        async getPosts() {
            const { data } = await this.http.get('https://jsonplaceholder.typicode.com/posts');
            return data;
        }
    }


    class PhotosService {
        constructor( private http: HttpClient){
            
        }
        async getPhotos() {
            const { data } = await this.http.get('https://jsonplaceholder.typicode.com/photos');
            return data;
        }

    }

    const http = new HttpClient();

    const todoService = new TodoService(http);
    const postService = new PostService(http);
    const photosService = new PhotosService(http);
    
    
    const todos = await todoService.getTodoItems();
    const posts = await postService.getPosts();
    const photos = await photosService.getPhotos();
    
    
    console.log({ todos: todos.length, posts: posts.length, photos: photos.length });
    console.log(" ⬆️  - 👆  Principio de Open Closed 👆 - ⬆️ ");

    console.log("------------------------- TERMINA los logs de principio SOLID ---------------------------------------------");

})();