// - Principio de inversión de dependencia -
// Los modulos de alto nivel no deben importar o depender de los modulos de bajo nivel
// Las abstracciones no deben depender de los detalles, los detalles deben depender
// de abstracciones


interface Post {
    body:   string;
    id:     number;
    title:  string;
    userId: number;
}

import axios from "axios";
import JsonPosts from "./JsonDataBase.json"

abstract class PostProvider {
    abstract getPosts(): Promise<Post[]>
}

class JsonDataBaseService implements PostProvider {

    async getPosts() {
        return JsonPosts;
    }
}

class WebApiPostService implements PostProvider {

    async getPosts() {
            const { data, status } = await axios.get("https://jsonplaceholder.typicode.com/posts");
            console.log({ status })
            return data;   
    }
}
class LocalDataBaseService implements PostProvider {

    async getPosts() {
        return [
            {
                'userId': 1,
                'id': 1,
                'title': 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
                'body': 'quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto'
            },
            {
                'userId': 1,
                'id': 2,
                'title': 'qui est esse',
                'body': 'est rerum tempore vitae sequi sint nihil reprehenderit dolor beatae ea dolores neque fugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis qui aperiam non debitis possimus qui neque nisi nulla'
            }]
    }

}

class PostService {

    private posts: Post[] = [];

    constructor(private postProvider: PostProvider) {}

    async getPosts() {
        // const jsonDB = new LocalDataBaseService();
        // this.posts = await jsonDB.getFakePosts();

        // const jsonDB = new JsonDataBaseService();
        // this.posts = await jsonDB.getPosts();

        this.posts = await this.postProvider.getPosts()

        return this.posts;
    }
}

(async () => {

    const provider = new LocalDataBaseService()
    // const provider = new JsonDataBaseService()
    // const provider = new WebApiPostService()
    const postService = new PostService( provider );

    const posts = await postService.getPosts();

    console.log({ posts })


    console.log(" ⬆️  - 👆  Principio de Inversión de Dependencias 👆 - ⬆️ ");


})();