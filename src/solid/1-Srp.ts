(() => {

    // - Principio de Responsabilidad Única - 
    // - La clase o modulo debe tener solo un enfoque o responsabilidad, no puedes mezclar 
    // en una clase para usuarios que te regrese los Post[] o te regrese Categories[]
    // Este ejercicio es solo una representación de metodos o clases para mostrar un ejemplo

    interface Product { 
        id:   number;
        name: string;
    }

    // Clase para bade de datos en este caso enfocado a producto
    class ProductService {
        getProduct ( id: number) {
            // Realiza un proceso para obtener el producto y retornarlo desde base de datos
            console.log('Producto: ',{ id, name: 'OLED Tv' });
        }

        saveProduct( product: Product ) {
            // Realiza una petición para salvar en base de datos 
            console.log('Guardando en base de datos', product );
        }
    }
    

    // Clase de Email 
    class Mailer {
        // Método para enviar emails
        sendEmail(emailList: string[], template: "to-clients" | "to-admins") {
            console.log('Enviando correo a los clientes', template);
        }
    }

    // Clase para Sección de producto donde tenemos todos los métodos ordenados 
    // y separados en diferentes clases para no saturarla
    class ProductSection {
        private productService: ProductService;
        private mailer: Mailer;

        constructor(productService:ProductService, mailer: Mailer ){
            this.productService = productService;
            this.mailer = mailer;
        }
    
        loadProduct( id: number ) {
            // Realiza un proceso para obtener el producto y retornarlo
            this.productService.getProduct( id )
        }
    
        saveProduct( product: Product ) {
            // Realiza una petición para salvar en base de datos 
            this.productService.saveProduct( product )
        }
    
        notifyClients() {
            // Notifica al cliente en este caso a través de un email
            this.mailer.sendEmail(["abcd@globant.com","123@globant.com"], "to-clients")
        }      
    
    }

    // Clase de carrito para agregar items, eliminarlos, o calcular totales
    class CartSection {
        private itemsInCart: Object[] = [];
        onAddToCart( productId: number ) {
            // Agregar al carrito de compras
            console.log('Agregando al carrito ', productId );
        }
    }
    

    console.log("------------------------- COMIENZA los logs de principio SOLID ---------------------------------------------");
    
    const productService = new ProductService(); 
    const mailer = new Mailer();


    const productSection = new ProductSection(productService, mailer);
    const cartSection = new CartSection()

    productSection.loadProduct(10);
    productSection.saveProduct({ id: 10, name: 'OLED TV' });
    productSection.notifyClients();
    
    cartSection.onAddToCart(10);
    console.log("⬆️  - 👆  Principio de Responsabilidad Única 👆 - ⬆️ ");

})();