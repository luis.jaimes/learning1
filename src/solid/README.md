This folder contains my course practice

UDEMY: "Principios SOLID" by Fernando Herrera

SOLID is an acronym for the first five object-oriented design (OOD) principles by Robert C. Martin (also known as Uncle Bob).

- 1 Single-Responsibility Principle
- 2 Open-Closed Principle
- 3 Liskov Substitution Principle
- 4 Interface Segregation Principle
- 5 Dependency Inversion Principle
