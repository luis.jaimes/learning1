// Interface Segregation
// Los clientes no deberían estar obligados a depender de interfaces que no utilizan
// Una clase no debería implementar una funcion que no utiliza solo porque la interfaz
// la está obligando a ellos, para eso deberia hacerse una segregación de interfaz, es decir;
// se debería separar las interfaces para una mejor implementación



interface Bird {
    eat(): void;

    // Funciones segregadas
    // fly(): void;
    // run(): void;
    // swim(): void;
    // walk(): void;
}

interface FlyingBird {
    fly(): void;
}

interface RunningBird {
    run(): void;
}

interface SwimmingBird {
    swim(): void;
}

interface WalkingBird {
    walk(): void;
}

(() => {
  class Tucan implements Bird, FlyingBird, WalkingBird {
      public fly(){}
      public eat(){
      }
      public walk(){}
    }
  class Hummingbird implements Bird, FlyingBird {
    public fly(){}
    public eat(){
    }
  }  

  class Ostrich implements Bird, RunningBird, WalkingBird {
    public fly(){}
    public eat(){
    }
    public run(){}
    public walk(){}
  }

  class Penguin implements Bird, SwimmingBird, WalkingBird {
      eat(){
          return "Comer";
      }
      swim(){
        return "Nadar";
      }
      walk(){
        return "Caminar";
      }
  }

const penguin = new Penguin()

console.log(`El Pingüino puede: ${penguin.eat()}, ${penguin.swim()} y ${penguin.walk()}`);
console.log(" ⬆️  - 👆 Principio de segregación de interfaz 👆 - ⬆️ ");

})();


