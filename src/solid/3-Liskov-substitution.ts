// liskov substitution
// SI en alguna parte de nuestro código estamos usando una clase y creamos clases hijas  
// esas clases hijas podrán sustituir a la padre y que el código siga funcionando exactamente igual
// esto nos obliga a asegurarnos de que cuando extendemos una clase no estamos modificando
// el comportamiento de la clase padre

// En código creamos una clase abstracta Vehicle con un método abstracto para establecer el comportamiento de todas las clases que se
// quieran heredar, las cuales van a tener el método getNumberOfSeats() forzosamente implementado así si queremos realizar
// algún cambio solo se modificarían las clases hijas y tendríamos como referiencia la clase Vechicle como base

abstract class Vehicle {

    abstract getNumberOfSeats(): number;
}

class Tesla extends Vehicle {

    constructor( private numberOfSeats: number ) {
        super();
    }

    getNumberOfSeats() {
        return this.numberOfSeats;
    }
}

class Audi extends Vehicle {

    constructor( private numberOfSeats: number ) {
        super();
    }

    getNumberOfSeats() {
        return this.numberOfSeats;
    }
}

class Toyota extends Vehicle {

    constructor( private numberOfSeats: number ) {
        super();
    }

    getNumberOfSeats() {
        return this.numberOfSeats;
    }
}

class Honda extends Vehicle {

    constructor( private numberOfSeats: number ) {
        super();
    }

    getNumberOfSeats() {
        return this.numberOfSeats;
    }
}


(() => {
    
    const printCarSeats = ( cars: Vehicle[] ) => {
        
        // for (const car of cars) {
         
        //     if( car instanceof Tesla ) {
        //         console.log( 'Tesla', car.getNumberOfSeats() )
        //         continue;
        //     }
        //     if( car instanceof Audi ) {
        //         console.log( 'Audi', car.getNumberOfSeats() )
        //         continue;
        //     }
        //     if( car instanceof Toyota ) {
        //         console.log( 'Toyota', car.getNumberOfSeats() )
        //         continue;
        //     }
        //     if( car instanceof Honda ) {
        //         console.log( 'Honda', car.getNumberOfSeats() )
        //         continue;
        //     }         

        // }

        
        cars.forEach((car) => {
            console.log(car.constructor.name, car.getNumberOfSeats());
            
        })

        console.log(" ⬆️  - 👆 Principio de sustitución de Liskov 👆 - ⬆️ ");

    }
    
    const cars = [
        new Tesla(7),
        new Audi(2),
        new Toyota(5),
        new Honda(5),
    ];


    printCarSeats( cars );

})();


